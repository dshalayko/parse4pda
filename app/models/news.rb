class News < ApplicationRecord
  has_one :micropost
  default_scope -> {order(time: :desc)}

  def self.search(search)
    if search
      where('title LIKE ?', "%#{search}%")
    else
      all
    end

  end

end
