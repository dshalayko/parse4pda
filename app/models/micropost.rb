class Micropost < ApplicationRecord
  belongs_to :news
  validates :news_id, presence: true
end
