# frozen_string_literal: true

class NewsController < ApplicationController
  before_action :set_news, only: %i[show edit update destroy]
  SITE = "http://4pda.ru"
  # GET /news
  # GET /news.json
  def index
    @news = News.search(params[:search]).paginate(:page => params[:page])
  end

  # GET /news/1
  # GET /news/1.json
  def show
    @news = News.find(params[:id])
    @micropost = @news.micropost
  end

  # GET /news/new
  def newcontent
    @news = News.new
  end

  # GET /news/1/edit
  def edit;
  end

  def parsing
    begin
      parse_paginators(1)
    rescue => err
      render plain: err
    end
  end


  # POST /news
  # POST /news.json
  def create
    @news = News.new(news_params)

    respond_to do |format|
      if @news.save
        @micropost = Micropost.create(content: @news.title, url: @news.id, news_id: @news.id)
        format.html {redirect_to @news, notice: 'News was successfully created.'}
        format.json {render :show, status: :created, location: @news}
      else
        format.html {render :new}
        format.json {render json: @news.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /news/1
  # PATCH/PUT /news/1.json
  def update
    respond_to do |format|
      if @news.update(news_params)
        format.html {redirect_to @news, notice: 'News was successfully updated.'}
        format.json {render :show, status: :ok, location: @news}
      else
        format.html {render :edit}
        format.json {render json: @news.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /news/1
  # DELETE /news/1.json
  def destroy
    @news.micropost.destroy
    @news.destroy
    respond_to do |format|
      format.html {redirect_to news_index_url, notice: 'News was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  def parse_pages(url)
    posts_array = []
    agent = Mechanize.new
    posts = agent.get(url).search('.post')
    posts.each do |post|
      news = {}
      news[:title] = post.search('.list-post-title').text.downcase
      news[:description] = post.search("[@itemprop='description']").text
      news[:url] = 'http:' + post.search('a/@href').first.to_s
      news[:date] = post.search('meta/@content').text
      news[:img] = post.search('img/@src').text
      posts_array << news unless news[:title].nil?
    end
    posts_array
  end

  def parse_paginators(number)
    agent = Mechanize.new
    page = agent.get(SITE + "/news/page/" + number.to_s)
    current_page = page.search(".active").text.to_i
    next_page = current_page + 1
    puts current_page
    pages = parse_pages(SITE + "/news/page/" + number.to_s)

    pages.each do |h|
      if News.find_by title: h[:title]
      else
        @news = News.create(title: h[:title], time: h[:date], img: h[:img]) if h[:title] != ''
        @micropost = Micropost.create(content: h[:description], url: h[:url], news_id: @news.id) if h[:description] != ''
      end
    end

    #что бы не грузить все страницы раскоментировать 2 строки
    # return current_page unless current_page < 1
    # parse_paginators(next_page)

    if News.find_by time: pages.first[:date]
      current_page
    else
      Thread.new do
        parse_paginators(next_page)
      end
    end

  end


  private

  # Use callbacks to share common setup or constraints between actions.
  def set_news
    @news = News.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def news_params
    params.require(:news).permit(:title)
  end

end
