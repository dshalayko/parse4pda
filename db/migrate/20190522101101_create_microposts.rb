class CreateMicroposts < ActiveRecord::Migration[5.2]
  def change
    create_table :microposts do |t|
      t.text :content
      t.references :news, foreign_key: true
      t.text :url

      t.timestamps
    end

  end
end
