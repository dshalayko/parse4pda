class AddTimeToNews < ActiveRecord::Migration[5.2]
  def change
    add_column :news, :time, :string
  end
end
