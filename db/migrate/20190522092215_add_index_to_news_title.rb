class AddIndexToNewsTitle < ActiveRecord::Migration[5.2]
  def change
    add_index :news, :title, unique: true
  end
end
